# Curso de [**C# parte 01**: Primeiros passos](https://cursos.alura.com.br/course/csharp-parte-1-primeiros-passos) realizado na **[Alura](https://www.alura.com.br/)**

### Aula 01: História e ecossistema da linguagem
História e ecossistema da linguagem
- Apresentação
- Historia e ecossistema .NET
- Benefício da CLR
- Quais aplicações?
- Sobre o .NET Framework
- O que aprendemos?

### Aula 02 Nosso primeiro programa e o Visual Studio
- Nosso primeiro programa CSharp
- Sobre a compilação e execução
- Instalando o Visual Studio
- Mão na massa: instale a IDE Visual Studio
- Ola mundo no Visual Studio
- Sobre IDEs e editores
- Estrutura de diretorios do Visual Studio
- Entrada da aplicação
- Compilar e executar

Para compilar um programa manualmente basta executar o **csc.exe** e apontar o arquivo *(Ex: Programa.txt)*

```
c:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe Programa.txt
```