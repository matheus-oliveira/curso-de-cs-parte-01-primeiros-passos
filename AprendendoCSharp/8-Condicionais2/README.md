## Objetivos desse projeto
- Aprender operadores lógica
    - Operador de negação lógica !
    - Operador AND lógico &
    - Operador OR exclusivo lógico ^
    - Operador OR lógico |
    - Operador AND lógico condicional &&
    - Operador OR lógico condicional ||
    - Operador IGUALDADE ==