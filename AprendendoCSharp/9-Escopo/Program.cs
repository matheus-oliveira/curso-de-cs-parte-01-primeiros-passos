﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9_Escopo
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Executando projeto 9 - Escopo");

            int idadeJoao = 16;
            int quantidadePessoas = 2;
            string mensagemAdicional;
            bool acompanhado = quantidadePessoas >= 2;

            if (acompanhado == true)
            {
                mensagemAdicional = "João está acompanhado";
            }
            else
            {
                mensagemAdicional = "João NÃO está acompanhado";
            }



            if (idadeJoao <= 18 || acompanhado == true)
            {
                Console.WriteLine("Pode entrar. " + mensagemAdicional);
            }
            else
            {
                Console.WriteLine("Não pode entrar." + mensagemAdicional);
            }

            Console.WriteLine(@"



A execuçaõ acabou. Tecle enter para finalizar ...");
            Console.ReadLine();
        }

    }
}