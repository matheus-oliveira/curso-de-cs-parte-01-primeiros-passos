﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5_CaracteresETextos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("executando o projeto 5 - Caractere e textps");

            // Tipo character
            char primeiraLetra = 'a'; // para o tipo char deve usar apostrofe pois o C# identidfica as aspas como texto 
            Console.WriteLine(primeiraLetra);

            primeiraLetra = (char)65; // 65 é um numero tirado da tabela ASCII apos aconverção para o tipo caracter ele passa a assumir a letras "A"
            Console.WriteLine(primeiraLetra);

            primeiraLetra = (char)(primeiraLetra + 1);
            Console.WriteLine(primeiraLetra);

            string titulo = "Alura Cursos de tecnologia " + 2020;
            string cursosProgramacao = @"- .NET 
- Java
- Javascript"; // ao colocar @ na frente das aspas, o C# passa a interpretar as quebras de linha e todos os espaços
            Console.WriteLine(titulo);
            Console.WriteLine(cursosProgramacao);


            Console.WriteLine("A execuçaõ acabou. Tecle enter para finalizar ...");
            Console.ReadLine();
        }
    }
}
