## Objetivos desse projeto

- Aprender a diferença dos tipos de variaveis:
    - Double (permite numeros com virgula des que que )

    ```
    idade = 5 / 3;
    Console.WriteLine("5 / 3 = "+idade);

    idade = 5.0 / 3;
    Console.WriteLine("5.0 / 3 = " + idade);
    ```
    - Int