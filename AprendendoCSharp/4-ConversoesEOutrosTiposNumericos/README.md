## Objetivos desse projeto
- Aprender a diferença dos tipos de variaveis:
    - int (O int é uma variavel que suporta até 32 bits)
    - long (O long é uma variavel que suporta até 64 bits)
    - short (O short é uma variavel que suporta até 16 bits)
    - float (C# necessita que coloque o "f" para execurtar o codigo pois o float não é muito usado e ele entede que numeros com virgulas são double)
