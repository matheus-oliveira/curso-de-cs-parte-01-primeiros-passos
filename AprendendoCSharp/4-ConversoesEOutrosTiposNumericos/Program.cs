﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ConversoesEOutrosTiposNumericos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Execuntando o projeto 4");

            double salario;
            salario = 1200.60;
            Console.WriteLine(salario);
            
            int salarioInteriro; 
            salarioInteriro = (int)salario;
            Console.WriteLine(salarioInteriro);

            int quantidade = 15235; // O int é uma variavel que suporta até 32 bits
            Console.WriteLine(quantidade);

            long idade = 23; //O long é uma variavel que suporta até 64 bits 
            Console.Write(idade);

            short quantidadeProduto = 1600; // O short é uma variavel que suporta até 16 bits
            Console.Write(quantidadeProduto);

            float altura = 1.80f; // C# necessita que coloque o "f" para execurtar o codigo pois o float não é muito usado e ele entede que numeros com virgulas são double
            Console.Write(altura);

            Console.WriteLine("A execuçaõ acabou. Tecle enter para finalizar ...");
            Console.Read();
        }
    }
}
